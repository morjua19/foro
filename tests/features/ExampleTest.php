<?php

class ExampleTest extends FeatureTestCase
{

    function test_basic_example()
    {

        $user = factory(\App\User::class)->create([
            'name' => 'Juan Moreno',
            'email' => 'morjua19@gmail.com',
        ]);
        $this->actingAs($user, 'api')->visit('api/user')
             ->see('Juan Moreno')
            ->see('morjua19@gmail.com');
    }
}
